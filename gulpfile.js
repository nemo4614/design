var gulp = require('gulp'),
	sass = require('gulp-sass');

//Styles task
gulp.task('styles', function(){
	gulp.src('scss/**/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('css/'));
});

//Watch task
gulp.task('watch', function(){
	gulp.watch('scss/*.scss', ['styles']);
});